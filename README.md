# Curator
> A simple command line application to serve as demo from [this blog article]().

## Pre-requisites
You need .NET Core SDK 2.2 to install this application from the source.

## Installation from the source
Clone this repository and use this command from a terminal as the root level of this project

- YOUR_RUNTIME_IDENTIFIER: pick the appropriate [runtime identifier](https://docs.microsoft.com/fr-fr/dotnet/core/rid-catalog) for your system. For latest Ubuntu it should be `linux-x64`, for windows `win-x64` and for MacOs `osx-x64`.

- INSTALLATION_PATH: choose the directory where you want the application to be installed. For Ubuntu you could choose `$HOME/curator`.

```
dotnet publish src/Curator.Console -c Release --self-contained true -r [YOUR_RUNTIME_IDENTIFIER] -o [INSTALLATION_PATH]
```

Finally, add the installation directory to your `PATH` environment variable, for Ubuntu, simply execute the command:
```
export PATH=$PATH:[INSTALLATION_PATH]
```

## Installation from the binaries
Download and unzip the binaries for your system:
- [Windows x64](http://release.jbuisson.fr/curator-console/win-x64/latest.zip)
- [Linux x64](http://release.jbuisson.fr/curator-console/linux-x64/latest.zip)
- [OSX x64](http://release.jbuisson.fr/curator-console/osx-x64/latest.zip)

> For all other system, use the installation from source.