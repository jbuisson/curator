using System;
using Xunit;

namespace Curator.Data.Entities.Tests
{
    public class ItemTests
    {
        public class ToStringMethod
        {
            [Fact]
            public void ShouldRenderInformation()
            {
                var actual = new Item
                {
                    Id = 42,
                    Name = "Item Name",
                    CreatedAt = new DateTime(2019, 08, 06, 13, 37, 42),
                }.ToString();

                Assert.Equal("42: Item Name, 06/08/2019 13:37.", actual);
            }
        }
    }
}
