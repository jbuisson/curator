using System;
using System.Linq;
using Curator.Console.Commands;
using Curator.Core;
using Curator.Data.EntityFramework.Memory;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.AssertExtensions;

namespace Curator.Console.Tests
{
    public class AddCommandTests
    {
        public class OnExecuteMethod
        {
            [Fact]
            public async void ShouldThrowIfParentIsNull()
            {
                var subject = new AddCommand();
                await Assert.ThrowsAsync<ArgumentNullException>("Parent", subject.OnExecuteAsync);
            }

            [Fact]
            public async void ShouldThrowIfNameIsNull()
            {
                var subject = new AddCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);
                subject.Parent = new CuratorCommand(service);

                await Assert.ThrowsAsync<ArgumentNullException>("Name", subject.OnExecuteAsync);
            }

            [Fact]
            public async void ShouldAddNewItemWithName()
            {
                var subject = new AddCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);
                subject.Parent = new CuratorCommand(service);
                subject.Name = "ExpectedName";

                var result = await AssertDifference.CountAsync(context.Items, 1, subject.OnExecuteAsync);

                var actual = await context.Items.LastAsync();
                Assert.True(actual.Id != default(Int32));
                Assert.Equal("ExpectedName", actual.Name);
                Assert.True(actual.CreatedAt != default(DateTime));
                
                Assert.Equal(0, result);
            }
        }
    }
}
