export interface IItem {
  Id?: number;
  Name?: string;
  CreatedAt?: string;
}

export default IItem;
