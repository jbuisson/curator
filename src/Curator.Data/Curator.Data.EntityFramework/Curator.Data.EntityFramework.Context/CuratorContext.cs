﻿using System;
using Curator.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Curator.Data.EntityFramework.Context
{
    public class CuratorContext : DbContext
    {
        public DbSet<Item> Items { get; set; }

        public CuratorContext(DbContextOptions<CuratorContext> options) : base(options) { }
    }
}
