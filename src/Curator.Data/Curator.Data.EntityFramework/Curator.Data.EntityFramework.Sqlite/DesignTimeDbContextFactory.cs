﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Curator.Data.EntityFramework.Context;

namespace Curator.Data.EntityFramework.Sqlite
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CuratorContext>
    {
        private readonly IConfiguration m_configuration;

        public DesignTimeDbContextFactory()
        {
            m_configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional : false)
                .Build();
        }

        public DesignTimeDbContextFactory(IConfiguration configuration)
        {
            m_configuration = configuration;
        }

        public CuratorContext CreateDbContext(string[] args)
        {
            var datasource = m_configuration.GetValue<string>("DataSource");

            var builder = new DbContextOptionsBuilder<CuratorContext>()
                .UseSqlite($"Data Source={datasource}", b => b.MigrationsAssembly(GetType().Assembly.FullName));

            return new CuratorContext(builder.Options);
        }
    }
}
