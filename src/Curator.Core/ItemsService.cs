﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Curator.Data.Entities;
using Curator.Data.EntityFramework.Context;
using Curator.Core.Exceptions;

namespace Curator.Core
{
    public class ItemsService : IDisposable
    {
        private readonly CuratorContext m_context;

        public ItemsService(CuratorContext context)
        {
            m_context = context;
        }

        public async Task<Item> AddAsync(string name)
        {
            if (String.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            var entry = m_context.Add(new Item { Name = name });
            await m_context.SaveChangesAsync();

            return entry.Entity;
        }

        public async Task RemoveAsync(int id)
        {
            var item = await m_context.Items.FindAsync(id);

            if (item == null) throw new NotFoundException(id);

            m_context.Remove(item);
            await m_context.SaveChangesAsync();
        }

        public async Task<Item> FindAsync(int id)
        {
            var item = await m_context.Items.FindAsync(id);
            if (item == null) throw new NotFoundException(id);

            return item;
        }

        public async Task<IEnumerable<Item>> ListAsync()
        {
            return await m_context.Items.ToListAsync();
        }

        public void Dispose()
        {
            m_context.Dispose();
        }
    }
}
